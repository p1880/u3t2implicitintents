package dam.androidismael.u3t2implicitintents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MoreIntents extends AppCompatActivity implements View.OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_intents);
        setUI();

    }

    public void setUI(){
        Button btAjustes;

        btAjustes = findViewById(R.id.Ajustes);

        btAjustes.setOnClickListener(this);
    }

    public void openWifiSettings() {
        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.Ajustes:
                openWifiSettings();
                break;
        }
    }
}